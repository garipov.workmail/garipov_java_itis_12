import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		//объявление ссылки для входного массива
		int[] getData;
		//Запрос размера входного массива
		System.out.println("Enter size of array: ");
		int sizeArray = scanner.nextInt();
		//Создание входного массива с заданным размером
		getData = new int[sizeArray];
			//initialization
			for (int i = 0; i < sizeArray; i++){
				int someNumber = scanner.nextInt();
				getData[i] = someNumber;
			}	
		//Создание зеракального представления массива
		//Объявление ссылки на зеркальный массив
		int[] getMirrorData;
		//Создание зеркального массива с заданным размером
		getMirrorData = new int[sizeArray];
			//Initialuzation of getMirrorData array
			for (int i = 0, j = sizeArray - 1; i<sizeArray; i++, j--){
				getMirrorData[i] = getData[j];
				//Вывод зеркальных значений значения
				System.out.print("|" + getMirrorData[i] + " ");
			}
	}
}
