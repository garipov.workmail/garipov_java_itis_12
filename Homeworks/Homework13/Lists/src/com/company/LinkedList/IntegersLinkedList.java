package com.company.LinkedList;

import com.company.node.Node;
import com.sun.org.apache.xpath.internal.objects.XNodeSet;

public class IntegersLinkedList {
    Node first;
    Node last;

    private int count;

    public IntegersLinkedList() {
        this.count = 0;
    }

    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
//            Node current =first;
//            while (current.getNext() != null) {
//                current = current.getNext();
//            }
//            current.setNext(newNode);
            last.setNext(newNode);
            last = newNode;
        }
        count++;
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            Node current = first;
            for (int i = 1; i <= index; i++) {
                current = current.getNext();
            }
            return current.getValue();
        } else {
            System.out.println("Нет такого элемента");
            return -1;
        }
    }

    public void addToBegin(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            Node temp = first;
            first = newNode;
            first.setNext(temp);
        }
        this.count++;
    }

    public void remuveByIndex(int index) {
        Node current = first;

        if (index == 0) {
            first = current.next;
        }

        if (index >= 0 && index < count) {
            for (int i = 1; i <= index - 1; i++) {
                current = current.getNext();
                }
            current.setNext(current.next.next);
            count--;
            } else {
            System.out.println("Нет такого элемента");
            }
    }

    public void remuve(int element) {
        Node current = first;
        for (int i = 1; i <= count; i++) {
            if (element == current.getNext().getValue()) {
                if (current.next.next == null) {
                    current.setNext(null);
                    last = current;
                    count--;
                } else {
                    current.setNext(current.next.next);
                    count--;
                }
            } else if (first.getValue() == element) {
                first = current.next;
                count--;
            } else {
                current = current.next;
            }
        }
    }

    public boolean conntains(int element) {
        Node current = first;
        for (int i = 1; i <= count; i++) {
            if (element == current.getValue()){
                return true;
            }
            current = current.getNext();
        }
        return false;
    }

    public int size() {
        return count;
    }
}
