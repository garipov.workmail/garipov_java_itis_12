package com.company.LinkedList;

import com.company.node.Node;

public class IntegersLinkedListIterator {
    private Node current;

    public IntegersLinkedListIterator(IntegersLinkedList list) {current = list.first;}

    public boolean hasNext() {return  current != null;}

    public int next() {
        int value = current.getValue();
        current = current.getNext();
        return value;
    }
}
