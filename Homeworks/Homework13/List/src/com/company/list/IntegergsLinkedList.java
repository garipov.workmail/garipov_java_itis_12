package com.company.list;

public class IntegergsLinkedList {
    private Node first;
    private Node last;
    private int count;

    public IntegergsLinkedList(){
    this.count = 0;
    }

    public void add(int element){
        Node newNode = new Node(element);
        if (first == null) {
            this.first = newNode;
            this.last = newNode;
        } else {
//            Node current = first;
//            while (current.getNext() != null){
//                current = current.getNext();
//            }
//            current.setNext(newNode);
            this.last.setNext(newNode);
            this.last = newNode;
        }
        this.count++;
    }

    public int get(int index) {
        // TODO: реализовать получение элемента
        if (index > 0 && index <= count) {
            Node current = first;
            for (int i = 1; i < index; i++) {
                current = current.getNext();
            }
            return current.getValue();
        } else {
            System.out.println("Нет такого элемента");
            return -1;
        }
    }

    public void addIn(int element, int index){
        Node current = first;
        Node newNode = new Node (element);
        //Добавление элемента в начало списка
        if (index == 1) {
            first = newNode;
            newNode.setNext(current);
           count++;
        } else {
            //Добавление элемента в середину списка
            if (index > 1 && index <= count) {
                for (int i = 1; i <= index - 2; i++) {
                    current = current.getNext();
                }
                newNode.setNext(current.getNext());
                current.setNext(newNode);
                count++;
            } else {
                //Добавление элемента в конец списка
                if (index == count + 1) {
                    last.setNext(newNode);
                    last = newNode;
                    count++;
                } else {
                 //Ошибка добавления
                    System.err.println("Невозможно добавить элемент под этим индексом");
                }
            }
        }
    }

    public void removeByIndex (int index) {
        Node current = first;
        //Удаление по индексу 1-го значения списка
        if (index == 1) {
            first = current.getNext();
            count--;
        } else {
            //Удаление по индексу остальных значений списка
            if (index > 1 && index <= count - 1) {
                for (int i = 1; i <= index - 2; i++) {
                    current = current.getNext();
                }
                current.setNext(current.getNext().getNext());
                count--;
            } else {
                //Удаление последнего элемента
                if (index == count) {
                    for (int i = 1; i <= index - 2; i++) {
                        current = current.getNext();
                    }
                    current.setNext(current.getNext().getNext());
                    this.last = current;
                    count--;
                } else {
                    //Ошибка при вводе индекса элемента списка
                    System.err.println("Нет такого элемента");
                }
            }
        }
    }

    public void reverse (){
        Node temp = first;
        Node pointer = first;
        Node prev = null;
        Node current = null;

        while (pointer != null) {
            current = pointer;
            pointer = pointer.next;

            //разворот ссылки
            current.next = prev;
            prev = current;
            first = current;
        }
        last = temp;
    }

    public boolean contains(int element){
        Node current = first;
        for (int i = 1; i <= count; i++){
            if (element == current.getValue()){
                return true;
            }
            current = current.getNext();
        }
        return false;
    }

    public int size() {return count;}


}
