package com.company.list;

public class MainForIntegersArrayList {
    public static void main(String[] args) {
        IntegersArrayList list = new IntegersArrayList();

        list.add(7);
        list.add(6);
        list.add(12);
        list.add(16);
        list.add(20);
        list.add(25);
        list.add(22);
        list.add(3);
        list.add(5);
        list.add(35);
        list.add(51);
        list.add(52);
        list.reverse();
        list.remove(11);


        System.out.print(list.get(0) + " "); // метод get()
        System.out.print(list.get(1) + " "); // метод get()
        System.out.print(list.get(2) + " "); // метод get()
        System.out.print(list.get(3) + " "); // метод get()
        System.out.print(list.get(4) + " "); // метод get()
        System.out.print(list.get(5) + " "); // метод get()
        System.out.print(list.get(6) + " "); // метод get()
        System.out.print(list.get(7) + " "); // метод get()
        System.out.print(list.get(8) + " "); // метод get()
        System.out.print(list.get(9) + " "); // метод get()
        System.out.print(list.get(10) + " "); // метод get()
        System.out.println("\n" + list.size());

//        System.out.println("\nВ списке - " + list.size() + " элемент(ов).");



    }
}
