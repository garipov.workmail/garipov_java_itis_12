package array;

public class MainForArrayList {

    public static void main(String[] args) {
	    ArrayList list = new ArrayList();
	    list.add(10);
	    list.add(5);
	    list.add(7);
	    list.addToBegin(77);
		list.removeByIndex(3);
		list.remove(10);
		System.out.println(list.get(0));
		System.out.println(list.get(1));
		System.out.println(list.size());
		System.out.println(list.contains(5));
    }
}
