package array;

public class ArrayList {
    private static final int DEFAULT_ARRAY_SIZE = 10;

    private int elements[];
    private int count;

    public ArrayList() {
        this.elements = new int[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    public void add(int element) {
        if (count < DEFAULT_ARRAY_SIZE) {
            this.elements[count] = element;
            this.count++;
        } else {
            System.err.println("Нет места");
        }
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            System.err.println("Неверный индекс");
            return -1;
        }
    }

    public void addToBegin(int element) {
        if (count < DEFAULT_ARRAY_SIZE) {
            for (int i = count; i > 0; i--) {
                elements[i] = elements[i - 1];
            }
            this.elements[0] = element;
            this.count++;
        } else {
            System.err.println("Нет места");
        }
    }

    public void remove(int element) {
        // TODO: реализовать
        int numMoved = 0;
        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                numMoved = i;
            }
        }
        for (int i = numMoved; i < count; i++){
            elements[i] = elements[i+1];
            elements[count] = 0;
            count--;
        }
    }

    public void removeByIndex(int index) {
        // TODO: удалить по индексу
        for (int i = index; i < count; i++){
            elements[i] = elements[i+1];
            elements[count] = 0;
            count--;
        }


    }

    public boolean contains(int element) {
        // TODO: реализовать
        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        return count;
    }


}
