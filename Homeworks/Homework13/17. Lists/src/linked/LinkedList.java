package linked;

import nodes.Node;

public class LinkedList {
    Node first;
    private Node last;

    private int count;

    public LinkedList() {

    }

    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
//            Node current = first;
//            while (current.getNext() != null) {
//                current = current.getNext();
//            }
//            current.setNext(newNode);
            last.setNext(newNode);
            last = newNode;
        }
        count++;
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            Node current = first;
            for (int i = 1; i <= index; i++) {
                current = current.getNext();
            }
            return current.getValue();
        } else {
            System.out.println("Нет такого элемента");
            return -1;
        }
    }

    public void addToBegin(int element) {
        // TODO: реализовать
        Node newNode = new Node(element);
        if (first ==null) {
            first = newNode;
            last = newNode;
        } else {
            Node temp = first;
            first = newNode;
            first.setNext(temp);
            }
        count++;
    }

    public void remove(int element) {
        // TODO: реализовать

    }

    public void removeByIndex(int index) {
        // TODO: удалить по индексу
    }

    public boolean contains(int element) {
        // TODO: реализовать
        return false;
    }

    public int size() {
        return count;
    }
}
