package linked;

public class MainForLinkedList {
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.add(3);
        list.add(7);
        list.add(8);
        list.add(9);
        list.addToBegin(1);
        list.addToBegin(2);

//        System.out.println(list.get(0));
//        System.out.println(list.get(1));
//        System.out.println(list.get(2));
//        System.out.println(list.get(3));

//        for (int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i));
//        }

        LinkedListIterator iterator = new LinkedListIterator(list);
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
