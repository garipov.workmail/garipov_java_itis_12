import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int number = 0;
		int sum = 0;
		for (number = scanner.nextInt(); number != 0; number /= 10) {
			sum += (number % 10);
		}
		System.out.println(sum);
	}
}