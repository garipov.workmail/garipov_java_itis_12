import java.util.Scanner;
class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
			//Объявляем целочисленную переменную, для проверки значений через цикл
			int currentNumber = 0;
			int currentNumberMultiplication = 1;
			int sumNumber = 0;
			//Запуск цикла, который выводит на экран числа, до ввода "-1"
			currentNumber = scanner.nextInt();
			while (currentNumber != -1) {
				//Запуск второго цикла, для перемножения цифр числа
				while (currentNumber != 0) {
					currentNumberMultiplication *= currentNumber % 10;
					currentNumber /=  10; 
				}
				//Проверка условия кратности трём
				if (currentNumberMultiplication % 3 == 0) {
					sumNumber += currentNumberMultiplication;
				}
				//Вывод информации о текущем итоге
			System.out.println("Current total: " + sumNumber);
			currentNumberMultiplication = 1;
			currentNumber = scanner.nextInt();
			}
	}
}
