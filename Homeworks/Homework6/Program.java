/*Для массива вида int a[] = {3, 2, 1, 6, 1}
 сохранить в переменную number число 32161*/

class Program{
	public static void main(String[] args) {
		int a[] = {3, 2, 1, 6, 1};
		int number = 0;
		int multiplier = 10000;
		for (int i = 0; i < 4; i++){
			number += a[i] * multiplier;
			multiplier /= 10;
		}
	System.out.println(number);
	}
}