import java.util.Scanner;

class Program {
	// Функция, возвращающуя сумму цифр определенного числа.
	public static int sumNumeral(int sumSomeNumeral) {
		int result = 0;
		while (sumSomeNumeral != 0) {
			result += sumSomeNumeral % 10;
			sumSomeNumeral /= 10;
		}
		return result;
	}
	
	//Функция возвращающая зеркальное представление числа.
	public static int mirrorNumber(int mirrorSomeNumber) {
		int result = 0;
		//Массив для хранения числа в зеркальном отражении
		int[] tempMirror;
		//Переменная для хранения длины числа
		int numLength = 0;
		int temp = 0;
		//Переменная для хранения разрядности числа
		double multiplier = 0.1;
		//Временная переменная temp нужная для дальнейшего определения длины числа
		temp = mirrorSomeNumber;
			//Цикл для определения длины числа
			while (temp != 0){
				temp /= 10;
				numLength++;
				multiplier *= 10;
			}
			// Создание массива с размером numLength, с зеркальной записью mirrorSomeNumber
			tempMirror = new int[numLength];
			//Массив с зеркальной записью числа mirrorSomeNumber
			for (int i = 0; i < numLength; i ++){
				tempMirror[i] = mirrorSomeNumber % 10;
				mirrorSomeNumber /= 10;
			}
			//Преобразование зеркального массива в число
			for (int i = 0; i < numLength; i++){
			result += tempMirror[i] * multiplier;
			multiplier /= 10;
		}
		return result;
	}

	//Функция, которая возвращает сумму чисел, которые находятся в диапазоне от a до b.
	public static int sumNumbers(int a, int b){
		int sum = 0;
		if (a < b) {
					for (int i = a; i < b+1; i++) {
					sum += i;
				}
		} else {
					for (int i = a; i > b-1; i--){
						sum += i;
					}
				}
	return sum;
	}

	public static void main(String[] args) {
	Scanner scanner = new Scanner(System.in);	
	System.out.println("Enter some number... ");
	int enterNumber = scanner.nextInt();
	//Применение функции sumNumeral
	int additionResult = sumNumeral(enterNumber);
	//Применение функции mirrorNumber
	int mirrorResult = mirrorNumber(enterNumber);
	System.out.println("Sum numeral of a number - " + additionResult);
	System.out.println("Mirror numeral of a number - " + mirrorResult);
	//Применение функции sumNumbers
	System.out.println("Enter some number A... ");
	int enterNumberA = scanner.nextInt();
	System.out.println("Enter some number B... ");
	int enterNumberB = scanner.nextInt();
	int sumResult = sumNumbers(enterNumberA,enterNumberB);
	System.out.println("A + B - " + sumResult);
	}
}