public class Program {
    public static int parseInt(char number[]) {
        //Целочисленная переменная для хранения ASCII кода числа
        int codeOfNumber = 0;
        //Цикл парсера
        for (int i = 0; i < number.length; i++){
            //Переменная для хранения разряда числа
            double multipier = 0.1;
            int numLenght = number.length - i;
            //Цикл для определения разряда числа
            while (numLenght != 0){
                multipier *= 10;
                numLenght--;
            }
            //преобразование ASCII кода в int с учетом разряда
            codeOfNumber += (number[i] - 48) * multipier;
        }
        return codeOfNumber;
    }
    public static void main(String[] args) {
        char n[] = {'3', '2', '4'};
        int x = parseInt(n); // x = 324
        System.out.println(x);
    }
}
