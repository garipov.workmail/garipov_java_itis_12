package com.company;
import java.util.Random;
public class RemoteController {
    private char controllerModel[];
    private TV tv;

    RemoteController(char controllerModel[]){
        this.controllerModel = controllerModel;
    }

    void setTVcontroll(TV tv){
        this.tv = tv;
    }

    void on(){
        Random random = new Random();
        tv.onChannel(random.nextInt(3));
    }
}
