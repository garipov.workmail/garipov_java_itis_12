package com.company;
import java.util.Random;
public class TV {
    private final int MAX_CHANNEL = 5;

    private char model[];
    private Channel channels[];
    private RemoteController remoteController;
    private int count;

    TV(char model[]){
        this.model = model;
        this.channels = new Channel[MAX_CHANNEL];
        this.count = 0;
    }

    void addChannel(Channel channel){
        if(count < MAX_CHANNEL){
            this.channels[count] = channel;
            channel.setTV(this);
            this.count++;
        }
    }

    void setRemoteController(RemoteController remoteController){
        this.remoteController = remoteController;
    }

    void onChannel (int num){
        Random random = new Random();
        this.channels[num].channelOn();
        this.channels[num].onProgram(random.nextInt(3));
    }
}
