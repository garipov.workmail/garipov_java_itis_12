package com.company;

public class Channel {

    private final int MAX_PROGRAM = 5;

    private char channelTitle[];
    private Program programs[];
    private int count;
    private TV tv;

    Channel (char channelTitle[]){
        this.channelTitle = channelTitle;
        this.programs = new Program[MAX_PROGRAM];
        this.count = 0;
    }

    void setTV(TV tv){
        this.tv = tv;
    }

    void addProgram(Program program){
        if (count < MAX_PROGRAM){
            this.programs[count] = program;
            program.setChannel(this);
            this.count++;
        }
    }

    void onProgram (int num){
        programs[num].programOn();
    }

    void channelOn(){
        System.out.print(this.channelTitle);
        System.out.print(" - канал включен\n");
    }

}
