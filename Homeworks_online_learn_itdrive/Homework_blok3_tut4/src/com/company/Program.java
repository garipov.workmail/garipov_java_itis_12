package com.company;

public class Program {
    private char programTitle[];
    private Channel channel;

    Program (char programTitle[]){
        this.programTitle = programTitle;
    }

    void setChannel(Channel channel){
        this.channel = channel;
    }

    void programOn(){
        System.out.print(this.programTitle);
        System.out.print(" - программа включена\n");
    }
}
