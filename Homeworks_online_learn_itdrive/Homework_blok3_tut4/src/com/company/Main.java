package com.company;

public class Main {

    public static void main(String[] args) {
        Program program1 =  new Program("Morning news".toCharArray());
        Program program2 =  new Program("Daily news".toCharArray());
        Program program3 =  new Program("Evening news".toCharArray());
        Program program4 =  new Program("Morning match".toCharArray());
        Program program5 =  new Program("Daily match".toCharArray());
        Program program6 =  new Program("Evening match".toCharArray());
        Program program7 =  new Program("Morning oil price".toCharArray());
        Program program8 =  new Program("Daily oil price".toCharArray());
        Program program9 =  new Program("Evening oil price".toCharArray());

        Channel channel1 = new Channel("NEWS".toCharArray());
        Channel channel2 = new Channel("SPORT".toCharArray());
        Channel channel3 = new Channel("BUSINESS".toCharArray());

        TV tv = new TV("SomeTv".toCharArray());

        RemoteController remoteController = new RemoteController("SomeController".toCharArray());

        channel1.addProgram(program1);
        channel1.addProgram(program2);
        channel1.addProgram(program3);
        channel2.addProgram(program4);
        channel2.addProgram(program5);
        channel2.addProgram(program6);
        channel3.addProgram(program7);
        channel3.addProgram(program8);
        channel3.addProgram(program9);

        tv.addChannel(channel1);
        tv.addChannel(channel2);
        tv.addChannel(channel3);

        tv.setRemoteController(remoteController);
        remoteController.setTVcontroll(tv);

        remoteController.on();

    }
}
