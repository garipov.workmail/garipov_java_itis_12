package java12;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private String address;

    private User(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.address = builder.address;
    }

    public static class Builder {
        private String firstName;
        private String lastName;
        private int age;
        private String address;

        public Builder firstName(String firstname){
            this.firstName = firstname;
            return this;
        }

        public Builder lastName(String lastname){
            this.lastName = lastname;
            return this;
        }

        public Builder age(int age){
            this.age = age;
            return this;
        }

        public Builder address(String address){
            this.address = address;
            return this;
        }

        public User build(){
            return new User(this);
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public static Builder builder() {
        return new Builder();
    }
}
