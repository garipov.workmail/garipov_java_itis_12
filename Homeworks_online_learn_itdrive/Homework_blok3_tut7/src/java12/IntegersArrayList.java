package java12;

public class IntegersArrayList {
    private static final int DEFAULT_ARRAY_SIZE = 10;

    private int elements[];

    private int count;

    class ArrayListIterator {
        int current;
        ArrayListIterator(){
        this.current = 0;
        }
        boolean hasNext(){
            return current < count;
        }

        int next() {
            int nextElement = elements[current];
            current++;
            return nextElement;
        }
    }

    public IntegersArrayList() {
        this.elements = new int[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    public void add(int element) {
        if (count < this.elements.length) {
            this.elements[count] = element;
            count++;
        } else {
            int oldData[] = new int[DEFAULT_ARRAY_SIZE];
            oldData = elements;
            elements = new int[DEFAULT_ARRAY_SIZE * 3 / 2 + 1];
            System.arraycopy(oldData, 0, elements, 0, size());
            this.elements[count] = element;
            count++;
        }
    }


    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            System.out.println("Index out of range");
            return -1;
        }
    }

    public int getCount() {
        return count;
    }

    public void addByIndex(int element, int index) {
        if (index >= 0 && index < count) {
            //запомнить элемент на место которого будет вставлен новый
            int temp = this.elements[index];
            //сдвинуть массив вправо на одно значение
            count++;
            int i = count;
            while (i != index) {
                elements[i] = elements[i - 1];
                i--;
            }
            //перезаписать элемент
            this.elements[index] = element;
        }
    }


    public void remove(int index) {
        if (index >= 0 && index < count) {
            //сдвиг массива
            for (int i = index; i < count; i++) {
                elements[i] = elements[i + 1];
            }
            //уменьщение размера списка
            count--;
        }
    }

    public void reverse() {
        int firstPointer = 0;
        int lastPointer = count-1;
        while (firstPointer != count/2) {
            //запомнить первый элемент списка
            int tempFirst = elements[firstPointer];
            //запомнить последний элемент спика
            int tempLast = elements[lastPointer];
            //присвоить значение последнего элемента
            elements[firstPointer] = tempLast;
            elements[lastPointer] = tempFirst;
            lastPointer--;
            firstPointer++;
        }
    }

    public IntegersArrayList addAsBuilder(int value) {
        this.add(value);
        return this;
    }

    public int size() {
        return count;
    }
}
