package ru.org.showmefiles.app;

import com.org.showmefiles.ShowMeFiles;
import com.beust.jcommander.JCommander;


public class Main {

    public static void main(String[] args) {
        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);
        ShowMeFiles showMeFiles = new ShowMeFiles();
        showMeFiles.print(arguments.path);
    }
}