package ru.organization.printer.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
class Arguments {

	@Parameter(names = {"--black"})
	public String black;

	@Parameter(names = {"--white"})
	public String white;

}