package com.company;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Human humans[] = new Human[100];
        for (int i = 0; i < 100; i++) {
            humans[i] = new Human(random.toString().toCharArray(), random.nextInt(70));
            System.out.println("Name: " + humans[i].name + "\tAge: " + humans[i].age);
        }

        int count[] = new int[71];
        for (int i = 0; i <99; i++) {
            count[humans[i].age]++;
        }
        int max = 0;
        int num = 0;
        //ниже находим макстмальное количество повторов
        for (int i = 0; i < 71; i++) {
            if (count[i] > max) {
                max = count[i];
            }
        }
        for (int i = 0; i < 71; i++) {
            if (count[i] == max) {
                num = i;
            }
        }
        System.out.println("Люди в возрасте - " + num + " лет." + " Встречаются " + max +" раз.");
    }
    }
