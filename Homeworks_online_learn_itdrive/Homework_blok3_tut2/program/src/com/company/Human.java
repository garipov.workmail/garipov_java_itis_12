package com.company;
import java.util.Random;
public class Human {
    char name[];
    int age;

    public Human(char[] name, int age) {
        this.name = name;
        this.age = age;
    }

    public char[] getName() {
        return name;
    }

    public void setName(char[] name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
