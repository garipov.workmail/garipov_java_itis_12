package com.company.resources;

public class Card {
    private int ammount;

    public Card(int ammount) {
        this.ammount = ammount;
    }

    public int getAmmount() {
        return ammount;
    }

    public boolean buy(int cost) {
        if (cost <=ammount) {
            this.ammount -= cost;
            return true;
        }else {
            System.out.println("Недостаточно средств!");
            return false;
        }
    }
}
