package com.company.resources;

public class Main {
    public static void main(String[] args) {
        Card card = new Card(1000);
        Human husband = new Human("Муж", card);
        Human wife = new Human("Жена", card);

        husband.start();
        wife.start();
    }
}
