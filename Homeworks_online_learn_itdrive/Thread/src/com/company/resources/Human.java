package com.company.resources;

public class Human extends Thread {
    private String name;
    private final Card card;

    public Human(String name, Card card) {
        this.name = name;
        this.card = card;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            synchronized (card) {

                if (card.getAmmount() > 0) {
                    System.out.println(name + " покупает ");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        throw new IllegalArgumentException(e);
                    }
                    if (card.buy(10)) {
                        System.out.println(name + " купил");
                    } else {
                        System.out.println(name + " говорит ЭЭЭЭ");
                    }
                }
            }

        }
    }
}
